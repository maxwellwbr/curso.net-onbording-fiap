﻿
    class Calculadora : ICalculadora
    {
        private double Num1 { get; set; }
        private double Num2 { get; set; }

        public Calculadora(double num1, double num2)
        {
            this.Num1 = num1;
            this.Num2 = num2;
        }

        public void Somar()
        {
            Console.WriteLine(this.Num1 + this.Num2);
        }  

        public void  Subtrair()
        {
            Console.WriteLine(this.Num1 - this.Num2);
        }

        public void Multiplicar()
        {
            Console.WriteLine( this.Num1 * this.Num2);
        }

        public void Dividir()
        {
            if( this.Num2 == 0)
            {
                 Console.WriteLine("Divisão não pode ser por 0" );
                 return;
            }

            Console.WriteLine( this.Num1 / this.Num2);
        }
    }
